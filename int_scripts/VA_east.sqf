//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			VA_east.sqf
// Author:			Kamaradski 2014
// Contributers:	Razgriz33
//
// 
// Drop-in Virtual Arsenal ammo-box for "Transport & Ambush" & I&A
// _K = [this] execVM "int_scripts\VA_east.sqf"; from the init of any object.
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

sleep 5; // allow other modules to load first

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Setup VA system
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

_myBox = _this select 0;

_myBox allowDamage false; 
_myBox enableSimulation false;

clearWeaponCargo _myBox;
clearMagazineCargo _myBox;
clearBackpackCargo _myBox;
clearItemCargo _myBox;

["AmmoboxInit",[_myBox,true]] call BIS_fnc_arsenal;


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Empty VA system
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

[_myBox,[true],true] call BIS_fnc_removeVirtualBackpackCargo;
[_myBox,[true],true] call BIS_fnc_removeVirtualItemCargo;
[_myBox,[true],true] call BIS_fnc_removeVirtualWeaponCargo;
[_myBox,[true],true] call BIS_fnc_removeVirtualMagazineCargo;


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Add side independent gear
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

[_myBox,[
	"V_Chestrig_oli",
	"H_Shemag_olive",
	"ItemMap",
	"ItemCompass",
	"ItemWatch",
	"ItemRadio",
	"V_BandollierB_blk",
	"H_Cap_oli",
	"V_Chestrig_blk",
	"H_Watchcap_blk",
	"V_TacVest_blk",
	"H_Booniehat_khk",
	"G_Goggles_VR",
	"H_Bandanna_khk",
	"H_Watchcap_camo",
	"V_BandollierB_khk",
	"ItemGPS"
],true] call BIS_fnc_addVirtualItemCargo;	

[_myBox,[
	"LMG_M200",
	"Rangefinder",
	"Laserdesignator",
	"arifle_SDAR_F",
	"arifle_TRG21_F",
	"arifle_TRG20_F",
	"arifle_TRG20_ACO_F",
	"hgun_ACPC2_F",
	"Binocular",
	"arifle_Mk20_GL_ACO_F",
	"LMG_Mk200_F",
	"arifle_Mk20_F",
	"arifle_Mk20C_ACO_F",
	"arifle_TRG21_GL_F",
	"arifle_Mk20_MRCO_F",
	"arifle_TRG21_MRCO_F"
],true] call BIS_fnc_addVirtualWeaponCargo;	
	
	
[_myBox,[
	"Chemlight_blue",
	"9Rnd_45ACP_Mag",
	"SmokeShellRed",
	"SmokeShellBlue",
	"1Rnd_HE_Grenade_shell",
	"1Rnd_Smoke_Grenade_shell",
	"1Rnd_SmokeGreen_Grenade_shell",
	"1Rnd_SmokeRed_Grenade_shell",
	"1Rnd_SmokeBlue_Grenade_shell",
	"APERSMine_Range_Mag",
	"HandGrenade",
	"MiniGrenade",
	"16Rnd_9x21_Mag",
	"1Rnd_HE_Grenade_shell",
	"1Rnd_SmokeOrange_Grenade_shell",
	"SmokeShellOrange",
	"SmokeShellRed",
	"ClaymoreDirectionalMine_Remote_Mag",
	"APERSTripMine_Wire_Mag",
	"Laserbatteries",
	"6Rnd_LG_scalpel",
	"2Rnd_GBU12_LGB",
	"100Rnd_127x99_mag_Tracer_Yellow"
],true] call BIS_fnc_addVirtualMagazineCargo;


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Add OPFOR gear
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

[_myBox,[
	"O_HMG_01_weapon_F",
	"O_GMG_01_weapon_F",
	"O_HMG_01_high_weapon_F",
	"O_GMG_01_high_weapon_F",
	"O_Mortar_01_weapon_F",
	"B_AssaultPack_rgr",
	"B_BergenC_grn",
	"B_Carryall_khk",
	"B_FieldPack_khk",
	"B_Kitbag_rgr",
	"B_OutdoorPack_blu",
	"B_TacticalPack_rgr",
	"B_Parachute"
],true] call BIS_fnc_addVirtualBackpackCargo;
	

[_myBox,[
	"U_OG_Guerilla1_1",
	"U_OG_leader",
	"U_OG_Guerilla2_1",
	"U_OG_Guerilla2_3",
	"U_OG_Guerilla2_2",
	"U_OG_Guerilla3_1",
	"U_OG_Guerrilla_6_1",
	"U_O_OfficerUniform_ocamo",
	"H_Beret_ocamo",
	"U_O_PilotCoveralls",
	"V_HarnessO_brn",
	"H_HelmetO_ocamo",
	"U_O_CombatUniform_ocamo",
	"H_MilCap_ocamo",
	"V_HarnessOGL_brn",
	"H_HelmetLeaderO_ocamo",
	"V_TacVest_khk",
	"H_PilotHelmetHeli_O",
	"U_O_SpecopsUniform_ocamo",
	"H_HelmetCrew_O",
	"H_PilotHelmetFighter_O",
	"H_CrewHelmetHeli_O",
	"V_TacVest_brn",
	"O_UavTerminal",
	"U_O_Wetsuit",
	"V_RebreatherIR",
	"G_O_Diving",
	"U_O_GhillieSuit",
	"V_Chestrig_khk",
	"V_HarnessOSpec_brn",
	"H_HelmetSpecO_ocamo",
	"H_HelmetSpecO_blk",
	"U_O_CombatUniform_oucamo",
	"V_HarnessO_gry",
	"H_HelmetO_oucamo",
	"V_HarnessOGL_gry",
	"H_HelmetLeaderO_oucamo",
	"U_O_Protagonist_VR"
],true] call BIS_fnc_addVirtualItemCargo;


[_myBox,[
	"arifle_Katiba_C_ACO_F",
	"hgun_Pistol_heavy_02_Yorris_F",
	"arifle_Katiba_ACO_pointer_F",
	"hgun_Rook40_F",
	"arifle_Katiba_ACO_F",
	"arifle_Katiba_GL_ACO_F",
	"LMG_Zafir_pointer_F",
	"arifle_Katiba_ARCO_pointer_F",
	"arifle_Katiba_GL_ARCO_pointer_F",
	"srifle_DMR_01_DMS_F",
	"arifle_Katiba_pointer_F",
	"arifle_Katiba_C_ACO_pointer_F",
	"SMG_02_ACO_F",
	"arifle_Katiba_C_F",
	"hgun_Rook40_snds_F",
	"arifle_Katiba_ARCO_F",
	"srifle_GM6_camo_LRPS_F",
	"arifle_Katiba_ACO_pointer_snds_F",
	"srifle_DMR_01_DMS_snds_F",
	"arifle_Katiba_C_ACO_pointer_snds_F",
	"arifle_Katiba_GL_ACO_pointer_snds_F",
	"arifle_Katiba_ARCO_pointer_snds_F"
],true] call BIS_fnc_addVirtualWeaponCargo;


[_myBox,[
	"30Rnd_65x39_caseless_green",
	"6Rnd_45ACP_Cylinder",
	"Chemlight_red",
	"30Rnd_65x39_caseless_green",
	"30Rnd_65x39_caseless_green",
	"1Rnd_SmokeYellow_Grenade_shell",
	"150Rnd_762x51_Box",
	"30Rnd_65x39_caseless_green_mag_Tracer",
	"O_IR_Grenade",
	"SmokeShellYellow",
	"10Rnd_762x51_Mag",
	"30Rnd_9x21_Mag",
	"30Rnd_65x39_caseless_green",
	"Chemlight_red",
	"Chemlight_red",
	"5Rnd_127x108_Mag",
	"5Rnd_127x108_APDS_Mag",
	"30Rnd_65x39_caseless_green"
],true] call BIS_fnc_addVirtualMagazineCargo;


// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Add MODDED gear
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
[_myBox,[
	"AGM_Altimeter",
	"AGM_SpareBarrel",
	"AGM_NVG_Gen1",
	"AGM_NVG_Gen2",
	"AGM_NVG_Gen4",
	"AGM_NVG_Wide",
	"AGM_Box_Medical",
	"AGM_Bandage",
	"AGM_Morphine",
	"AGM_Epipen",
	"AGM_Bloodbag",
	"AGM_MapTools",
	"AGM_UAVBattery",
	"AGM_EarBuds",
	"AGM_DefusalKit",
	"AGM_CableTie",
	"AGM_IR_Strobe_Item",
	"AGM_muzzle_mzls_H",
	"AGM_muzzle_mzls_B",
	"AGM_muzzle_mzls_L",
	"AGM_muzzle_mzls_smg_01",
	"AGM_muzzle_mzls_smg_02",
	"rhs_acc_tgpa",				// Muzzle Attachments
	"rhs_acc_1p29",				// Scope Attachments
	"rhs_acc_1p63",				// Scope Attachments
	"rhs_acc_ekp1",				// Scope Attachments
	"rhs_acc_1pn93_1",			// Scope Attachments
	"rhs_acc_1pn93_2",			// Scope Attachments
	"rhs_acc_pgo7v",				// Scope Attachments
	"rhs_acc_pkas",				// Scope Attachments
	"rhs_acc_pso1m2",				// Scope Attachments
	"rhs_balaclava",				// AFRF Facewear
	"rhs_scarf",					// AFRF Facewear
	"rhs_6b27m_green",			// AFRF Headwear
	"rhs_6b27m_green_ess",
	"rhs_6b27m_green_bala",
	"rhs_6b27m_green_ess_bala",
	"rhs_6b27m",
	"rhs_6b27m_ess",
	"rhs_6b27m_bala",
	"rhs_6b27m_ess_bala",
	"rhs_6b27m_digi",
	"rhs_6b27m_digi_bala",
	"rhs_6b27m_ml",
	"rhs_6b28_green",
	"rhs_6b28_green_ess",
	"rhs_6b28_green_bala",
	"rhs_6b28_green_ess_bala",
	"rhs_6b28",
	"rhs_6b28_ess",
	"rhs_6b28_bala",
	"rhs_6b28_ess_bala",
	"rhs_6b28_flora",
	"rhs_6b28_flora_ess",
	"rhs_6b28_flora_bala",
	"rhs_6b28_ess_bala",
	"rhs_Booniehat_flora",
	"rhs_Booniehat_digi",
	"rhs_fieldcap",
	"rhs_fieldcap_helm",
	"rhs_fieldcap",
	"rhs_fieldcap_helm_digi",
	"rhs_tsh4",
	"rhs_tsh4_ess",
	"rhs_tsh4_bala",
	"rhs_tsh4_ess_bala",
	"rhs_zsh7a_mike",
	"rhs_zsh7a",
	"rhs_uniform_flora",					// AFRF Uniforms
	"rhs_uniform_vdv_flora",
	"rhs_uniform_flora_patchless",
	"rhs_uniform_vdv_mflora",
	"rhs_uniform_mflora_patchless",
	"rhs_uniform_msv_emr",
	"rhs_uniform_vdv_emr",
	"rhs_uniform_emr_patchless",
	"rhs_uniform_df15",
	"rhs_6b23",							// AFRF Vests
	"rhs_6b23_crew",
	"rhs_6b23_crewofficer",
	"rhs_6b23_engineer",
	"rhs_6b23_medic",
	"rhs_6b23_rifleman",
	"rhs_6b23_sniper",
	"rhs_6b23_6sh92",
	"rhs_6b23_6sh92_radio",
	"rhs_6b23_6sh92_vog",
	"rhs_6b23_6sh92_vog_headset",
	"rhs_6b23_6sh92_headset",
	"rhs_6b23_6sh92_headset_mapcase",
	"rhs_6b23_digi",
	"rhs_6b23_digi_crew",
	"rhs_6b23_digi_crewofficer",
	"rhs_6b23_digi_engineer",
	"rhs_6b23_digi_medic",
	"rhs_6b23_digi_rifleman",
	"rhs_6b23_digi_sniper",
	"rhs_6b23_digi_6sh92",
	"rhs_6b23_digi_6sh92_radio",
	"rhs_6b23_digi_6sh92_vog",
	"rhs_6b23_digi_6sh92_vog_headset",
	"rhs_6b23_digi_6sh92_headset",
	"rhs_6b23_digi_6sh92_headset_mapcase",
	"rhs_6b23_ML",
	"rhs_6b23_ML_crew",
	"rhs_6b23_ML_crewofficer",
	"rhs_6b23_ML_engineer",
	"rhs_6b23_ML_medic",
	"rhs_6b23_ML_rifleman",
	"rhs_6b23_ML_sniper",
	"rhs_6b23_ML_6sh92",
	"rhs_6b23_ML_6sh92_radio",
	"rhs_6b23_ML_6sh92_vog",
	"rhs_6b23_ML_6sh92_vog_headset",
	"rhs_6b23_ML_6sh92_headset",
	"rhs_6b23_ML_6sh92_headset_mapcase",
	"rhs_6sh46",
	"rhs_vest_commander",
	"rhs_vydra_3m"
],true] call BIS_fnc_addVirtualItemCargo;

[_myBox,[
	"rhs_weap_igla",				// The 9K38 Igla is a man-portable surface-to-air missile guided by infrared.
	"rhs_weap_ak103",				// The AK-103 it's a Russian Assault rifle.
	"rhs_weap_ak103_npz",			// The AK-103 it's a Russian Assault rifle.
	"rhs_weap_ak103_1",			// The AK-103 it's a Russian Assault rifle.
	"rhs_weap_ak103_2",			// The AK-103 it's a Russian Assault rifle.
	"rhs_weap_ak74m",				// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_weap_ak74m_plummag",		// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_weap_ak74m_npz",			// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_weap_ak74m_folded",		// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_weap_ak74m_camo",			// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_weap_ak74m_desert",		// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_weap_ak74m_2mag",			// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_weap_ak74m_2mag_npz",		// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_weap_ak74m_2mag_camo",	// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_weap_ak74m_gp25",			// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_weap_ak74m_gp25_npz",		// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_weap_akm",				// The AKM it's a Russian Assault rifle.
	"rhs_weap_akms",				// The AKM it's a Russian Assault rifle.
	"rhs_weap_makarov_pmm",		// The Makarov PMM is one of thethe Russian standard military-issue side arm.
	"rhs_weap_pya",				// The MP-443 Grach is the Russian standard military-issue side arm.
	"rhs_weap_pkm",				// The PKM it's a Russian Machine-gun.
	"rhs_weap_pkp",				// The PKP Pecheneg is a variant of the Russian made PKM 7.62x54mmR medium machine gun
	"rhs_weap_rpg26",				// The RPG-26 is a man-portable, anti-tank rocket-propelled grenade launcher.
	"rhs_weap_rpg7",				// The RPG-7 is a man-portable, anti-tank rocket-propelled grenade launcher.
	"rhs_weap_rshg2",				// The RShG-2 is a variant of the RPG-26 armed with a thermobaric warhead.
	"rhs_weap_rsp30_white",		// The RSP 30mm is one of the Russian issued single-use flares.
	"rhs_weap_rsp30_green",		// The RSP 30mm is one of the Russian issued single-use flares.
	"rhs_weap_rsp30_red",			// The RSP 30mm is one of the Russian issued single-use flares.
	"rhs_weap_svdp",				// The SVD Dragunov is a Russian semi-automatic sniper rifle
	"rhs_weap_svdp_wd",			// The SVD Dragunov is a Russian semi-automatic sniper rifle
	"rhs_weap_svds",				// The SVD Dragunov is a Russian semi-automatic sniper rifle
	"rhs_weap_svdp_npz",			// The SVD Dragunov is a Russian semi-automatic sniper rifle
	"rhs_weap_svdp_wd_npz",		// The SVD Dragunov is a Russian semi-automatic sniper rifle
	"rhs_weap_svds_npz"			// The SVD Dragunov is a Russian semi-automatic sniper rifle
],true] call BIS_fnc_addVirtualWeaponCargo;

[_myBox,[
	"AGM_HandFlare_Green",
	"AGM_HandFlare_Red",
	"AGM_HandFlare_White",
	"AGM_HandFlare_Yellow",
	"AGM_M84",
	"AGM_M26_Clacker",
	"AGM_DeadManSwitch",
	"AGM_Clacker",
	"AGM_30Rnd_65x39_caseless_mag_Tracer_Dim",
	"AGM_30Rnd_65x39_caseless_mag_SD",
	"AGM_30Rnd_65x39_caseless_mag_AP",
	"AGM_30Rnd_65x39_caseless_green_mag_Tracer_Dim",
	"AGM_30Rnd_65x39_caseless_green_mag_SD",
	"AGM_30Rnd_65x39_caseless_green_mag_AP",
	"AGM_30Rnd_556x45_Stanag_Tracer_Dim",
	"AGM_30Rnd_556x45_Stanag_SD",
	"AGM_30Rnd_556x45_Stanag_AP",
	"AGM_20Rnd_762x51_Mag_Tracer",
	"AGM_20Rnd_762x51_Mag_Tracer_Dim",
	"AGM_20Rnd_762x51_Mag_SD",
	"AGM_20Rnd_762x51_Mag_AP",
	"AGM_20Rnd_762x51_Mag_LR",
	"rhs_30Rnd_762x39mm",				// The AK-103 it's a Russian Assault rifle.
	"rhs_30Rnd_762x39mm_tracer",		// The AK-103 it's a Russian Assault rifle.
	"rhs_30Rnd_762x39mm_89",			// The AK-103 it's a Russian Assault rifle.
	"rhs_30Rnd_545x39_AK",				// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_30Rnd_545x39_AK_green",		// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_30Rnd_545x39_7n10_AK",		// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_30Rnd_545x39_7n22_AK",		// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_45Rnd_545X39_AK",				// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_45Rnd_545X39_AK_Green",		// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_45Rnd_545X39_7N10_AK",		// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_45Rnd_545X39_7N22_AK",		// the AK-74M it's the modernization variant of the AK-74 Russian assault rifle
	"rhs_30Rnd_762x39mm",				// The AKM it's a Russian Assault rifle.
	"rhs_30Rnd_762x39mm_tracer",		// The AKM it's a Russian Assault rifle.
	"rhs_30Rnd_762x39mm_89",			// The AKM it's a Russian Assault rifle.
	"rhs_VOG25",						// The GP-25 Kostyor (which means Bonfire in Russian) is a Russian under barrel grenade launcher for the AK-series
	"rhs_VOG25p",						// The GP-25 Kostyor (which means Bonfire in Russian) is a Russian under barrel grenade launcher for the AK-series
	"rhs_vg40tb",						// The GP-25 Kostyor (which means Bonfire in Russian) is a Russian under barrel grenade launcher for the AK-series
	"rhs_g_vg40sz",					// The GP-25 Kostyor (which means Bonfire in Russian) is a Russian under barrel grenade launcher for the AK-series
	"rhs_vg40op_white",				// The GP-25 Kostyor (which means Bonfire in Russian) is a Russian under barrel grenade launcher for the AK-series
	"rhs_vg40op_green",				// The GP-25 Kostyor (which means Bonfire in Russian) is a Russian under barrel grenade launcher for the AK-series
	"rhs_vg40op_red",					// The GP-25 Kostyor (which means Bonfire in Russian) is a Russian under barrel grenade launcher for the AK-series
	"rhs_GRD40_white",				// The GP-25 Kostyor (which means Bonfire in Russian) is a Russian under barrel grenade launcher for the AK-series
	"rhs_GRD40_green",				// The GP-25 Kostyor (which means Bonfire in Russian) is a Russian under barrel grenade launcher for the AK-series
	"rhs_GRD40_red",					// The GP-25 Kostyor (which means Bonfire in Russian) is a Russian under barrel grenade launcher for the AK-series
	"rhs_mag_9x19_17",				// The Makarov PMM is one of thethe Russian standard military-issue side arm.
	"rhs_mag_9x19_17",				// The MP-443 Grach is the Russian standard military-issue side arm.
	"rhs_100Rnd_762x54mmR",			// The PKM it's a Russian Machine-gun.
	"rhs_100Rnd_762x54mmR_green",		// The PKM it's a Russian Machine-gun.
	"rhs_100Rnd_762x54mmR",			// The PKP Pecheneg is a variant of the Russian made PKM 7.62x54mmR medium machine gun
	"rhs_100Rnd_762x54mmR_green",		// The PKP Pecheneg is a variant of the Russian made PKM 7.62x54mmR medium machine gun
	"rhs_mag_rgd5",					// Grenades
	"rhs_mag_rdg2_white",				// Grenades
	"rhs_mag_rdg2_black",				// Grenades
	"rhs_mag_nspn_yellow",				// Grenades
	"rhs_mag_nspn_red",				// Grenades
	"rhs_mag_nspn_green",				// Grenades
	"rhs_mag_nspd",					// Grenades
	"rhs_mag_fakel",					// Grenades
	"rhs_mag_fakels",					// Grenades
	"rhs_mag_zarya2",					// Grenades
	"rhs_mag_plamyam",				// Grenades
	"rhs_mag_rsp30_white",				// RSP-30 Flare
	"rhs_mag_rsp30_red",				// RSP-30 Flare
	"rhs_mag_rsp30_green",				// RSP-30 Flare
	"rhs_10Rnd_762x54mmR_7N1" 			// The SVD Dragunov is a Russian semi-automatic sniper rifle
],true] call BIS_fnc_addVirtualMagazineCargo;

[_myBox,[
	"AGM_NonSteerableParachute",
	"rhs_assault_umbts",
	"rhs_assault_umbts_engineer",
	"rhs_sidor",
	"rhs_rpg"
],true] call BIS_fnc_addVirtualBackpackCargo;



// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// 	Remove globally restricted gear
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-	

[_myBox,[
	"hgun_PDW2000_F",
	"arifle_Mk20C_F",
	"arifle_Mk20_GL_F",
	"arifle_Mk20_plain_F",
	"arifle_Mk20C_plain_F",
	"arifle_Mk20_GL_plain_F",
	"srifle_EBR_F",
	"srifle_GM6_F",
	"launch_I_Titan_F",
	"launch_I_Titan_short_F",
	"launch_Titan_F",
	"launch_Titan_short_F"
],true] call BIS_fnc_removeVirtualWeaponCargo;	
	
	
[_myBox,[
	"B_HMG_01_support_F",
	"B_HMG_01_support_high_F",
	"B_Mortar_01_support_F",
	"I_HMG_01_weapon_F",
	"I_GMG_01_weapon_F",
	"I_HMG_01_high_weapon_F",
	"I_GMG_01_high_weapon_F",
	"I_HMG_01_support_F",
	"I_HMG_01_support_high_F",
	"I_Mortar_01_weapon_F",
	"I_Mortar_01_support_F",
	"O_HMG_01_support_F",
	"O_GMG_01_support_high_F",
	"O_Mortar_01_support_F",
	"B_UAV_01_backpack_F",
	"O_UAV_01_backpack_F",
	"I_UAV_01_backpack_F"
],true] call BIS_fnc_removeVirtualBackpackCargo;


[_myBox,[
	"NLAW_F",
	"Titan_AT",
	"Titan_AP",
	"Titan_AA",
	"RPG32_F",
	"RPG32_HE_F",
	"Titan_AT",
	"Titan_AP",
	"Titan_AA"
],true] call BIS_fnc_removeVirtualMagazineCargo;
	

[_myBox,[
	"optic_NVS",
	"optic_Nightstalker",
	"optic_tws",
	"optic_tws_mg",
	"U_I_CombatUniform",
	"U_I_CombatUniform_shortsleeve",
	"U_I_CombatUniform_tshirt",
	"U_I_OfficerUniform",
	"U_I_GhillieSuit",
	"U_I_HeliPilotCoveralls",
	"U_I_pilotCoveralls",
	"U_I_Wetsuit",
	"U_IG_Guerilla1_1",
	"U_IG_Guerilla2_1",
	"U_IG_Guerilla2_2",
	"U_IG_Guerilla2_3",
	"U_IG_Guerilla3_1",
	"U_IG_Guerilla3_2",
	"U_IG_leader",
	"U_BG_Guerilla3_2",
	"U_OG_Guerilla3_2",
	"U_C_Poloshirt_blue",
	"U_C_Poloshirt_burgundy",
	"U_C_Poloshirt_stripped",
	"U_C_Poloshirt_tricolour",
	"U_C_Poloshirt_salmon",
	"U_C_Poloshirt_redwhite",
	"U_C_Commoner1_1",
	"U_C_Commoner1_2",
	"U_C_Commoner1_3",
	"U_C_Poor_1",
	"U_C_Poor_2",
	"U_C_Scavenger_1",
	"U_C_Scavenger_2",
	"U_C_Farmer",
	"U_C_Fisherman",
	"U_C_WorkerOveralls",
	"U_C_FishermanOveralls",
	"U_C_WorkerCoveralls",
	"U_C_HunterBody_grn",
	"U_C_HunterBody_brn",
	"U_C_Commoner2_1",
	"U_C_Commoner2_2",
	"U_C_Commoner2_3",
	"U_C_PriestBody",
	"U_C_Poor_shorts_1",
	"U_C_Poor_shorts_2",
	"U_C_Commoner_shorts",
	"U_C_ShirtSurfer_shorts",
	"U_C_TeeSurfer_shorts_1",
	"U_C_TeeSurfer_shorts_2",
	"U_NikosBody",
	"U_MillerBody",
	"U_KerryBody",
	"U_OrestesBody",
	"U_AttisBody",
	"U_AntigonaBody",
	"U_IG_Menelaos",
	"U_C_Novak",
	"U_OI_Scientist",
	"V_PlateCarrierIA1_dgtl",
	"V_PlateCarrierIA2_dgtl",
	"V_PlateCarrierIAGL_dgtl",
	"V_RebreatherIA",
	"V_TacVest_oli",
	"V_TacVest_camo",
	"V_TacVest_blk_POLICE",
	"V_TacVestIR_blk",
	"V_TacVestCamo_khk",
	"V_BandollierB_cbr",
	"V_BandollierB_oli",
	"H_HelmetIA",
	"H_HelmetIA_net",
	"H_HelmetIA_camo",
	"H_HelmetCrew_I",
	"H_CrewHelmetHeli_I",
	"H_PilotHelmetHeli_I",
	"H_PilotHelmetFighter_I",
	"H_Booniehat_dgtl",
	"H_Booniehat_indp",
	"H_MilCap_dgtl",
	"H_Cap_grn",
	"H_Cap_red",
	"H_Cap_blu",
	"H_Cap_tan",
	"H_Cap_blk",
	"H_Cap_grn_BI",
	"H_Cap_blk_Raven",
	"H_Cap_blk_ION",
	"H_Cap_blk_CMMG",
	"H_MilCap_rucamo",
	"H_MilCap_gry",
	"H_MilCap_blue",
	"H_Booniehat_grn",
	"H_Booniehat_tan",
	"H_Booniehat_dirty",
	"H_StrawHat",
	"H_StrawHat_dark",
	"H_Hat_blue",
	"H_Hat_brown",
	"H_Hat_camo",
	"H_Hat_grey",
	"H_Hat_checker",
	"H_Hat_tan",
	"H_Bandanna_surfer",
	"H_Bandanna_cbr",
	"H_Bandanna_sgg",
	"H_Bandanna_gry",
	"H_Bandanna_camo",
	"H_TurbanO_blk",
	"H_Shemag_khk",
	"H_Shemag_tan",
	"H_ShemagOpen_khk",
	"H_ShemagOpen_tan",
	"H_Beret_blk",
	"H_Beret_blk_POLICE",
	"H_Beret_red",
	"H_Beret_grn",
	"H_Watchcap_khk",
	"H_Watchcap_sgg",
	"H_BandMask_blk",
	"H_BandMask_khk",
	"H_BandMask_reaper",
	"H_BandMask_demon",
	"H_Shemag_olive_hs",
	"H_Cap_oli_hs",
	"I_UavTerminal",
	"H_Booniehat_khk_hs"
],true] call BIS_fnc_removeVirtualItemCargo;

diag_log format ["%1: Transport & Ambush: EAST Virtual Arsenal initiated for: %2:", time, _myBox];