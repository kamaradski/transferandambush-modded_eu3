//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			ammo_box_rockets_blue.sqf
// Author:		Kamaradski 2014
// Contributers:	Jester
//
// Crate loading script with restricted ammo for "Transport & Ambush"
// This script will respawn the ammobox content every 30 min
// _K = [this] execVM "int_scripts\ammo_box_rockets_blue.sqf"; from the init of any object.
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

if (!isServer) exitWith {};

sleep 7; // allow other modules to load first

_myBox = _this select 0;
_myBox allowDamage false; 

while {true} do {
	diag_log format ["%1: Transport & Ambush: (re)Spawned restricted ammo BLUE-rockets:", time];
	clearWeaponCargoGlobal _myBox;
	clearMagazineCargoGlobal _myBox;
	clearBackpackCargoGlobal _myBox;
	clearItemCargoGlobal  _myBox;
	sleep 2;

	_myBox addMagazineCargoGlobal ["rhs_fgm148_magazine_AT",2];			// The FGM-148 Javelin is a U.S. fire-and-forget man portable anti-tank missile.
	_myBox addMagazineCargoGlobal ["rhs_fim92_mag",2];					// The FIM-92F Stinger is a man-portable surface-to-air infrared-guided missile.
	_myBox addMagazineCargoGlobal ["rhs_m136_mag",2];					// The M136 AT4 is a 84mm unguided portable single-shot anti-tank weapon.
	_myBox addMagazineCargoGlobal ["rhs_m136_hedp_mag",2];				// The M136 AT4 is a 84mm unguided portable single-shot anti-tank weapon.
	_myBox addMagazineCargoGlobal ["rhs_m136_hp_mag",2];				// The M136 AT4 is a 84mm unguided portable single-shot anti-tank weapon.


	sleep 1200; // Sleep 20 min before emptying the box, and respawn it's content
};
