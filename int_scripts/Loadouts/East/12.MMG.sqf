removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

player forceAddUniform "rhs_uniform_flora_patchless_alt";
player addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 6 do {player addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 3 do {player addItemToUniform "AGM_Morphine";};
player addItemToUniform "rhs_mag_rgd5";
player addItemToUniform "rhs_mag_rdg2_white";
player addVest "rhs_6b23_ML_6sh92";
player addItemToVest "rhs_100Rnd_762x54mmR";
player addItemToVest "rhs_mag_rgd5";
player addItemToVest "rhs_mag_rdg2_white";
for "_i" from 1 to 3 do {player addItemToVest "RH_18Rnd_9x19_VP";};
player addBackpack "rhs_sidor";
for "_i" from 1 to 3 do {player addItemToBackpack "rhs_100Rnd_762x54mmR";};
player addHeadgear "rhs_6b27m_ML_ess_bala";

player addWeapon "rhs_weap_pkm";
player addPrimaryWeaponItem "rhs_bipod";
player addWeapon "RH_vp70";

player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "ItemWatch";
player linkItem "tf_pnr1000a";