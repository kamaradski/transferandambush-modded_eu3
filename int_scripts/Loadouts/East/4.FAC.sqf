removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

player forceAddUniform "rhs_uniform_flora_patchless_alt";
player addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 6 do {player addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 3 do {player addItemToUniform "AGM_Morphine";};
for "_i" from 1 to 2 do {player addItemToUniform "AGM_CableTie";};
player addItemToUniform "AGM_MapTools";
for "_i" from 1 to 2 do {player addItemToUniform "RH_8Rnd_762_tt33";};
player addVest "rhs_6b23_ML_6sh92_headset";
for "_i" from 1 to 7 do {player addItemToVest "rhs_30Rnd_545x39_AK";};
player addBackpack "tf_mr3000_rhs";
for "_i" from 1 to 2 do {player addItemToBackpack "Laserbatteries";};
for "_i" from 1 to 2 do {player addItemToBackpack "rhs_mag_rgd5";};
for "_i" from 1 to 2 do {player addItemToBackpack "rhs_mag_rdg2_white";};
player addItemToBackpack "O_IR_Grenade";
player addHeadgear "rhs_fieldcap_helm_ml";

player addWeapon "rhs_weap_ak74m_npz";
player addPrimaryWeaponItem "rhs_acc_dtk";
player addPrimaryWeaponItem "FHQ_optic_VCOG";
player addWeapon "RH_tt33";
player addWeapon "Laserdesignator";

player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "ItemWatch";
player linkItem "tf_fadak";
player linkItem "ItemGPS";