removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

player forceAddUniform "rhs_uniform_flora_patchless_alt";
player addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 6 do {player addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 3 do {player addItemToUniform "AGM_Morphine";};
player addItemToUniform "rhs_mag_rgd5";
player addItemToUniform "rhs_mag_rdg2_white";
player addVest "rhs_6b23_ML_engineer";
for "_i" from 1 to 7 do {player addItemToVest "rhs_30Rnd_545x39_AK";};
player addBackpack "B_FieldPack_khk";
player addItemToBackpack "AGM_M26_Clacker";
player addItemToBackpack "ToolKit";
player addHeadgear "rhs_6b27m_ML_ess_bala";

player addWeapon "rhs_weap_ak74m_npz";
player addPrimaryWeaponItem "rhs_acc_dtk";

player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "ItemWatch";
player linkItem "tf_pnr1000a";