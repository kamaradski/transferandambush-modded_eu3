removeAllWeapons player;
removeAllItems player;
removeAllAssignedItems player;
removeUniform player;
removeVest player;
removeBackpack player;
removeHeadgear player;
removeGoggles player;

player forceAddUniform "rhs_uniform_flora_patchless_alt";
player addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 6 do {player addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 3 do {player addItemToUniform "AGM_Morphine";};
player addItemToUniform "rhs_mag_rgd5";
player addItemToUniform "rhs_mag_rdg2_white";
player addVest "rhs_6b23_ML_6sh92";
for "_i" from 1 to 8 do {player addItemToVest "rhs_30Rnd_545x39_AK";};
player addItemToVest "rhs_mag_rgd5";
player addHeadgear "rhs_6b27m_ml_bala";

player addWeapon "rhs_weap_ak74m_npz";
player addPrimaryWeaponItem "rhs_acc_dtk";

player linkItem "ItemMap";
player linkItem "ItemCompass";
player linkItem "ItemWatch";
player linkItem "tf_pnr1000a";